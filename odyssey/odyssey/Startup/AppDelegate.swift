//
//  AppDelegate.swift
//  odyssey
//
//  Created by Ravi Ranjan on 19/12/19.
//  Copyright © 2019 Ravi Ranjan. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window?.backgroundColor = .white
        return true
    }
}

