//
//  HomeTabbarVC.swift
//  odyssey
//
//  Created by Ravi Ranjan on 19/12/19.
//  Copyright © 2019 Ravi Ranjan. All rights reserved.
//

import UIKit

class HomeTabbarVC: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarControllerSetup()
        self.tabbarPrefrenceSetup()
    }
    
    fileprivate func tabbarPrefrenceSetup(){
        self.tabBar.tintColor = UIColor(red: 255.0 / 255.0, green: 137.0 / 255.0, blue: 147.0 / 255.0, alpha: 1)
    }
    
    fileprivate func tabBarControllerSetup(){
        print("\n Debugger : Tab Bar setup")
        
        
        /*
         Home view controller setup
         */
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        homeVC.tabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "home"), tag: 1)
        
        /*
         message view controller setup
         */
        
        let messageVC = self.storyboard?.instantiateViewController(withIdentifier: "MessageVC") as! MessageVC
        messageVC.tabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "chat"), tag: 2)
        
        /*
         Notification view controller setup
         
         */
        let calendarVC = self.storyboard?.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
        calendarVC.tabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "notification"), tag: 3)
        
        /*
         Profile view controller setup
         */
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        profileVC.tabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "profile"), tag: 4)
        
        /*
         Add stories view controller setup
         */
        let storiesVC = self.storyboard?.instantiateViewController(withIdentifier: "AddStoriesVC") as! AddStoriesVC
        storiesVC.tabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "gmailLogo"), tag: 5)
        
        
        let controllers = [homeVC,calendarVC,storiesVC,messageVC,profileVC]
        self.viewControllers = controllers
    }
}
