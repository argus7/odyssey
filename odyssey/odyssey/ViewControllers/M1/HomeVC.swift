//
//  HomeVC.swift
//  odyssey
//
//  Created by Ravi Ranjan on 19/12/19.
//  Copyright © 2019 Ravi Ranjan. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet weak var postTV: UITableView!
    @IBOutlet weak var featuredCV: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.registerCV()
        // Do any additional setup after loading the view.
    }
    func registerCV(){
        
        self.postTV.dataSource = self
        self.postTV.delegate = self
        self.featuredCV.dataSource = self
        self.featuredCV.delegate = self
        postTV.register(UINib(nibName: "PostTVCell", bundle: Bundle.main), forCellReuseIdentifier: "PostTVCell")
        featuredCV.register(UINib(nibName: "FeaturedCVCell", bundle: Bundle.main), forCellWithReuseIdentifier: "FeaturedCVCell")
    }
}


extension HomeVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 320
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let postCell = tableView.dequeueReusableCell(withIdentifier: "PostTVCell", for: indexPath) as! PostTVCell
        return postCell
    }
}

extension HomeVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
extension HomeVC : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let featureCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeaturedCVCell", for: indexPath) as! FeaturedCVCell
        return featureCell
    }
}
extension HomeVC : UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
  
        return CGSize(width: 80, height: 80)
    }
}
